﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace ProyectoTodoTicket.Controllers
{
    public class CarritoController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/Carrito/
        public IQueryable<Carrito> getCarrito()
        {
            return db.Carrito;

        }
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> getCarrito(int id)
        {
            Carrito carrito = await db.Carrito.FindAsync(id);
            if (carrito == null)
            {
                return NotFound();
            }
            return Ok(carrito);

        }
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> PostDCarrito(Carrito carrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Carrito.Add(carrito);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = carrito.Cantidad },
                carrito);

        }
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> PutCarrito(int id, Carrito carrito)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != carrito.CarritoId)
            {
                return BadRequest();
            }
            db.Entry(carrito).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = carrito.CarritoId },
                carrito);

        }
        [ResponseType(typeof(Carrito))]
        public async Task<IHttpActionResult> DeleteCarrito(int id)
        {
            Carrito eliminado = await db.Carrito.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Carrito.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.CarritoId },
                eliminado);

        }

    }
}
