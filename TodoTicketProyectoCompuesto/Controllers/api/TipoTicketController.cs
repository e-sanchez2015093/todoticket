﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class TipoTicketController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/TipoTicket/
        public IQueryable<TipoTicket> getTipoTickets()
        {
            return db.TipoTicket;

        }
        [ResponseType(typeof(TipoTicket))]
        public async Task<IHttpActionResult> getTipoTicket(int id)
        {
            TipoTicket tipoTicket = await db.TipoTicket.FindAsync(id);
            if (tipoTicket == null)
            {
                return NotFound();
            }
            return Ok(tipoTicket);

        }
        [ResponseType(typeof(TipoTicket))]
        public async Task<IHttpActionResult> PostTipoTicket(TipoTicket tipoTicket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.TipoTicket.Add(tipoTicket);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = tipoTicket.TipoTicketId },
                tipoTicket);

        }
        [ResponseType(typeof(TipoTicket))]
        public async Task<IHttpActionResult> PutTipoTicket(int id, TipoTicket tipoTicket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != tipoTicket.TipoTicketId)
            {
                return BadRequest();
            }
            db.Entry(tipoTicket).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = tipoTicket.TipoTicketId },
                tipoTicket);

        }
        [ResponseType(typeof(TipoTicket))]
        public async Task<IHttpActionResult> DeleteTipoTicket(int id)
        {
           TipoTicket eliminado = await db.TipoTicket.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.TipoTicket.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.TipoTicketId },
                eliminado);

        }

    }
}
