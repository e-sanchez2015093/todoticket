﻿
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class TicketController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/Ticket/
        public IQueryable<Ticket> getTicket()
        {
            return db.Ticket.Include(p => p.Evento).Include(r => r.TipoTicket).Include(l => l.LugarDistribucion).Include(p => p.Promociones);

        }
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> getTicket(int id)
        {
            Ticket ticket = await db.Ticket.FindAsync(id);
            if (ticket == null)
            {
                return NotFound();
            }
            return Ok(ticket);

        }
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> PostTicket(Ticket ticket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Ticket.Add(ticket);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = ticket.TicketId },
                ticket);

        }
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> PutTicket(int id, Ticket ticket)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != ticket.TicketId)
            {
                return BadRequest();
            }
            db.Entry(ticket).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = ticket.TicketId },
                ticket);

        }
        [ResponseType(typeof(Ticket))]
        public async Task<IHttpActionResult> DeleteTicket(int id)
        {
            Ticket eliminado = await db.Ticket.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Ticket.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.TicketId },
                eliminado); 

        }

    }
}
