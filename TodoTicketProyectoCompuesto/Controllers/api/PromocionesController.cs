﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class PromocionesController : ApiController
    {

        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/Promociones/
        public IQueryable<Promociones> getPromociones()
        {
            return db.Promociones;

        }
        [ResponseType(typeof(Promociones))]
        public async Task<IHttpActionResult> getPromocion(int id)
        {
            Promociones promociones = await db.Promociones.FindAsync(id);
            if (promociones == null)
            {
                return NotFound();
            }
            return Ok(promociones);

        }
        [ResponseType(typeof(Promociones))]
        public async Task<IHttpActionResult> PostPromociones(Promociones promociones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Promociones.Add(promociones);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = promociones.PromocionesId },
                promociones);

        }
        [ResponseType(typeof(Promociones))]
        public async Task<IHttpActionResult> PutPromociones(int id, Promociones promociones)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != promociones.PromocionesId)
            {
                return BadRequest();
            }
            db.Entry(promociones).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = promociones.PromocionesId },
                promociones);

        }
        [ResponseType(typeof(Promociones))]
        public async Task<IHttpActionResult> DeletePromociones(int id)
        {
            Promociones eliminado = await db.Promociones.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Promociones.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.PromocionesId },
                eliminado);

        }
    }
}
