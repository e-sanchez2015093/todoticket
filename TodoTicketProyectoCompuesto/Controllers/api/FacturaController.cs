﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class FacturaController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();

        //Get:api/Factura/
        public IQueryable<Factura> getFacturas()
        {
            return db.Factura;

        }
        [ResponseType(typeof(Factura))]
        public async Task<IHttpActionResult> getFactura(int id)
        {
            Factura factura = await db.Factura.FindAsync(id);
            if (factura == null)
            {
                return NotFound();
            }
            return Ok(factura);

        }
        [ResponseType(typeof(Factura))]
        public async Task<IHttpActionResult> PostFactura(Factura factura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Factura.Add(factura);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = factura.FacturaId },
                factura);

        }
        [ResponseType(typeof(Factura))]
        public async Task<IHttpActionResult> PutFactura(int id, Factura factura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != factura.FacturaId)
            {
                return BadRequest();
            }
            db.Entry(factura).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = factura.FacturaId },
                factura);

        }
        [ResponseType(typeof(Factura))]
        public async Task<IHttpActionResult> DeleteFactura(int id)
        {
            Factura eliminado = await db.Factura.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Factura.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.FacturaId },
                eliminado);

        }
    }
}
