﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class UsuarioController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();

        public IQueryable<Usuario> getUsuarios()
        {
            return db.Usuario.Include(c => c.TipoUsuario);


        }
        [ResponseType(typeof(Usuario))]
        
        public async Task<IHttpActionResult> getUsuario(int id)
        {
            Usuario usuario = await db.Usuario.FindAsync(id);
            if (usuario == null)
            {
                return NotFound();
            }
            return Ok(usuario);

        }
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> PostUsuaurio(Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Usuario.Add(usuario);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = usuario.UsuarioId },
                usuario);

        }
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> PutUsuario(int id, Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != usuario.UsuarioId)
            {
                return BadRequest();
            }
            db.Entry(usuario).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);

        }
        [ResponseType(typeof(Usuario))]
        public async Task<IHttpActionResult> DeleteUsuario(int id)
        {
            Usuario eliminado = await db.Usuario.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Usuario.Remove(eliminado);
            await db.SaveChangesAsync();
            return Ok(eliminado);


        }

        private bool ContactoExists(int id)
        {
            return db.Usuario.Count(e => e.UsuarioId == id) > 0;
        }


    }

}

