﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class DetalleFacturaController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/DetalleFactura/
        public IQueryable<DetalleFactura> getDetalles()
        {
            return db.DetalleFactura;

        }
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> getDetalleFactura(int id)
        {
            DetalleFactura detalleFactura = await db.DetalleFactura.FindAsync(id);
            if (detalleFactura == null)
            {
                return NotFound();
            }
            return Ok(detalleFactura);

        }
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> PostDetalleFactura(DetalleFactura detalleFactura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.DetalleFactura.Add(detalleFactura);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = detalleFactura.DetalleFacturaId },
                detalleFactura);

        }
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> PutDetalleFactura(int id, DetalleFactura detalleFactura)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != detalleFactura.DetalleFacturaId)
            {
                return BadRequest();
            }
            db.Entry(detalleFactura).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = detalleFactura.DetalleFacturaId },
                detalleFactura);

        }
        [ResponseType(typeof(DetalleFactura))]
        public async Task<IHttpActionResult> DeleteDetalleFactura(int id)
        {
            DetalleFactura eliminado = await db.DetalleFactura.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.DetalleFactura.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.DetalleFacturaId },
                eliminado);

        }

    }
}
