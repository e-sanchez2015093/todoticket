﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class StaffController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();

        public IQueryable<Staff> getStaffs()
        {
            return db.Staff;
        }
        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> getStaff(int id)
        {
            Staff sta = await db.Staff.FindAsync(id);
            if (sta == null)
            {
                return NotFound();
            }
            return Ok(sta);

        }
        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> PostStaff(Staff staff)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Staff.Add(staff);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                   new { id = staff.StaffId},
                   staff);

        }
        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> PutStaff(int id, Staff staff)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);

            }
            if (id != staff.StaffId)
            {
                return BadRequest();
            }
            db.Entry(staff).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);



        }

        [ResponseType(typeof(Staff))]
        public async Task<IHttpActionResult> DeleteStaff(int id)
        {
            Staff eliminado = await db.Staff.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Staff.Remove(eliminado);
            await db.SaveChangesAsync();
            return Ok(eliminado);

        }
        private bool ContactoExists(int id)
        {
            return db.Staff.Count(e => e.StaffId == id) > 0;
        }


    }
}
