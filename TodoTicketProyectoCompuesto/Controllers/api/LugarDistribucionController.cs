﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class LugarDistribucionController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/LugarDistribucion/
        public IQueryable<LugarDistribucion> getLugarDistribucion()
        {
            return db.LugarDistribucion;

        }
        [ResponseType(typeof(LugarDistribucion))]
        public async Task<IHttpActionResult> getLugar(int id)
        {
            LugarDistribucion lugarDistribucion = await db.LugarDistribucion.FindAsync(id);
            if (lugarDistribucion == null)
            {
                return NotFound();
            }
            return Ok(lugarDistribucion);

        }
        [ResponseType(typeof(LugarDistribucion))]
        public async Task<IHttpActionResult> PostLugarDistribucion(LugarDistribucion lugarDistribucion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.LugarDistribucion.Add(lugarDistribucion);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = lugarDistribucion.LugarDistribucionId },
                lugarDistribucion);

        }
        [ResponseType(typeof(LugarDistribucion))]
        public async Task<IHttpActionResult> PutLugarDistribucion(int id, LugarDistribucion lugarDistribucion)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != lugarDistribucion.LugarDistribucionId)
            {
                return BadRequest();
            }
            db.Entry(lugarDistribucion).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = lugarDistribucion.LugarDistribucionId },
                lugarDistribucion);

        }
        [ResponseType(typeof(LugarDistribucion))]
        public async Task<IHttpActionResult> DeleteLugarDistribucion(int id)
        {
            LugarDistribucion eliminado = await db.LugarDistribucion.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.LugarDistribucion.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.LugarDistribucionId },
                eliminado);

        }

    }
}
