﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class EventoController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/Evento/
        public IQueryable<Evento> getTipo()
        {
            return db.Evento.Include(c => c.Tipo).Include(i => i.Usuario); 
            

        }
        [ResponseType(typeof(Evento))]
        public async Task<IHttpActionResult> getEvento(int id)
        {
            Evento evento = await db.Evento.FindAsync(id);
            if (evento == null)
            {
                return NotFound();
            }
            return Ok(evento);

        }
        [ResponseType(typeof(Evento))]
        public async Task<IHttpActionResult> PostEvento(Evento evento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Evento.Add(evento);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = evento.EventoId },
                evento);

        }
        [ResponseType(typeof(Tipo))]
        public async Task<IHttpActionResult> PutEvento(int id, Evento evento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != evento.EventoId)
            {
                return BadRequest();
            }
            db.Entry(evento).State = System.Data.Entity.EntityState.Modified;
            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);

        }
        [ResponseType(typeof(Evento))]
        public async Task<IHttpActionResult> DeleteEvento(int id)
        {
            Evento eliminado = await db.Evento.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Evento.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.EventoId },
                eliminado);

        }
        private bool ContactoExists(int id)
        {
            return db.Evento.Count(e => e.EventoId == id)>0;

        }
    }
}
