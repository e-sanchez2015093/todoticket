﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class TipoUsuarioController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();

        public IQueryable<TipoUsuario> getTipoTickets()
        {
            return db.TipoUsuario;

        }
        [ResponseType(typeof(TipoUsuario))]
        public async Task<IHttpActionResult> getTipoTicket(int id)
        {
            TipoUsuario tipoUsuario = await db.TipoUsuario.FindAsync(id);

            if (tipoUsuario == null)
            {
                return NotFound();
            }
            return Ok(tipoUsuario);

        }

        [ResponseType(typeof(TipoUsuario))]
        public async Task<IHttpActionResult> putTipoTicket(int id, TipoUsuario tipoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tipoUsuario.TipoUsuarioId)
            {
                return BadRequest();
            }

            db.Entry(tipoUsuario).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);


        }
        [ResponseType(typeof(TipoUsuario))]
        public async Task<IHttpActionResult> postTipoUsuario(TipoUsuario tipoUsuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.TipoUsuario.Add(tipoUsuario);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = tipoUsuario.TipoUsuarioId }, tipoUsuario);

        }
        [ResponseType(typeof(TipoUsuario))]
        public async Task<IHttpActionResult> deleteTipoUsuario(int id)
        {
            TipoUsuario tipo = await db.TipoUsuario.FindAsync(id);
            if (tipo == null)
            {
                return NotFound();
            }

            db.TipoUsuario.Remove(tipo);
            await db.SaveChangesAsync();

            return Ok(tipo);

        }
        private bool ContactoExists(int id)
        {
            return db.TipoUsuario.Count(e => e.TipoUsuarioId == id) > 0;
        }

    }
}
