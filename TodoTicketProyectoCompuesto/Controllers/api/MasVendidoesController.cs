﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using System.Web.Http.OData;
using System.Web.Http.OData.Routing;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    /*
    The WebApiConfig class may require additional changes to add a route for this controller. Merge these statements into the Register method of the WebApiConfig class as applicable. Note that OData URLs are case sensitive.

    using System.Web.Http.OData.Builder;
    using System.Web.Http.OData.Extensions;
    using TodoTicketProyectoCompuesto.Models;
    ODataConventionModelBuilder builder = new ODataConventionModelBuilder();
    builder.EntitySet<MasVendido>("MasVendidoes");
    builder.EntitySet<Ticket>("Ticket"); 
    config.Routes.MapODataServiceRoute("odata", "odata", builder.GetEdmModel());
    */
    public class MasVendidoesController : ODataController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();

        // GET: odata/MasVendidoes
        [EnableQuery]
        public IQueryable<MasVendido> GetMasVendidoes()
        {
            return db.MasVendido.Include(e=> e.Ticket);
        }





        [Route("api/mas/Ticket")]
        public List<Object> GetMasVen()
        {
            IQueryable<Ticket> tickets = (from tk in db.Ticket select tk);
            IQueryable<Evento> eventos = (from ev in db.Evento select ev);
            IQueryable<Usuario> usuarios = (from us in db.Usuario select us);
            List<Object> couts = new List<object>();
            foreach (Ticket tk in tickets)
            {
                foreach (Evento eventss in eventos)
                {
                    foreach (Usuario us in usuarios)
                    {
                        var data = new
                        {
                            count = (from ms in db.MasVendido where ms.TicketId == tk.TicketId where tk.EventoId == eventss.EventoId where eventss.UsuarioId == us.UsuarioId select ms).Count()
                                ,
                            Ticket = tk,
                            Evento = eventss,
                            Usuario = us
                        };
                        couts.Add(data);



                    }
                }

            }
            return couts;
        }








        // GET: odata/MasVendidoes(5)
        [EnableQuery]
        public SingleResult<MasVendido> GetMasVendido([FromODataUri] int key)
        {
            return SingleResult.Create(db.MasVendido.Where(masVendido => masVendido.MasVendidoId == key));
        }

        // PUT: odata/MasVendidoes(5)
        public async Task<IHttpActionResult> Put([FromODataUri] int key, Delta<MasVendido> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MasVendido masVendido = await db.MasVendido.FindAsync(key);
            if (masVendido == null)
            {
                return NotFound();
            }

            patch.Put(masVendido);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MasVendidoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(masVendido);
        }

        // POST: odata/MasVendidoes
        public async Task<IHttpActionResult> Post(MasVendido masVendido)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.MasVendido.Add(masVendido);
            await db.SaveChangesAsync();

            return Created(masVendido);
        }

        // PATCH: odata/MasVendidoes(5)
        [AcceptVerbs("PATCH", "MERGE")]
        public async Task<IHttpActionResult> Patch([FromODataUri] int key, Delta<MasVendido> patch)
        {
            Validate(patch.GetEntity());

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            MasVendido masVendido = await db.MasVendido.FindAsync(key);
            if (masVendido == null)
            {
                return NotFound();
            }

            patch.Patch(masVendido);

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MasVendidoExists(key))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return Updated(masVendido);
        }

        // DELETE: odata/MasVendidoes(5)
        public async Task<IHttpActionResult> Delete([FromODataUri] int key)
        {
            MasVendido masVendido = await db.MasVendido.FindAsync(key);
            if (masVendido == null)
            {
                return NotFound();
            }

            db.MasVendido.Remove(masVendido);
            await db.SaveChangesAsync();

            return StatusCode(HttpStatusCode.NoContent);
        }

        // GET: odata/MasVendidoes(5)/Ticket
        [EnableQuery]
        public SingleResult<Ticket> GetTicket([FromODataUri] int key)
        {
            return SingleResult.Create(db.MasVendido.Where(m => m.MasVendidoId == key).Select(m => m.Ticket));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool MasVendidoExists(int key)
        {
            return db.MasVendido.Count(e => e.MasVendidoId == key) > 0;
        }
    }
}
