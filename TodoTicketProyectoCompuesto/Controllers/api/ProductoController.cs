﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class ProductoController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/Producto/
        public IQueryable<Producto> getProdcuto()
        {
            return db.Producto;

        }
        [ResponseType(typeof(Producto))]
        public async Task<IHttpActionResult> getProducto(int id)
        {
            Producto producto = await db.Producto.FindAsync(id);
            if (producto == null)
            {
                return NotFound();
            }
            return Ok(producto);

        }
        [ResponseType(typeof(Producto))]
        public async Task<IHttpActionResult> PostProducto(Producto producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Producto.Add(producto);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = producto.ProductoId },
                producto);

        }
        [ResponseType(typeof(Producto))]
        public async Task<IHttpActionResult> PutProducto(int id, Producto producto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != producto.ProductoId)
            {
                return BadRequest();
            }
            db.Entry(producto).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
            return CreatedAtRoute("DefaultApi",
                new { id = producto.ProductoId },
                producto);

        }
        [ResponseType(typeof(Producto))]
        public async Task<IHttpActionResult> DeleteProducto(int id)
        {
            Producto eliminado = await db.Producto.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Producto.Remove(eliminado);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = eliminado.ProductoId },
                eliminado);

        }
    }
}
