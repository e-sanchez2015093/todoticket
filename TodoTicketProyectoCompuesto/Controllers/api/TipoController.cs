﻿
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers.api
{
    public class TipoController : ApiController
    {
        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        //Get:api/Tipo/
        public IQueryable<Tipo> getTipo()
        {
            return db.Tipo;

        }
        [ResponseType(typeof(Tipo))]
        public async Task<IHttpActionResult> getTipo(int id)
        {
            Tipo tipo = await db.Tipo.FindAsync(id);
            if (tipo == null)
            {
                return NotFound();
            }
            return Ok(tipo);

        }
        [ResponseType(typeof(Tipo))]
        public async Task<IHttpActionResult> PostTipo(Tipo tipo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            db.Tipo.Add(tipo);
            await db.SaveChangesAsync();
            return CreatedAtRoute("DefaultApi",
                new { id = tipo.TipoId },
                tipo);

        }
        [ResponseType(typeof(Tipo))]
        public async Task<IHttpActionResult> PutTipo(int id, Tipo tipo)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (id != tipo.TipoId)
            {
                return BadRequest();
            }
            db.Entry(tipo).State = System.Data.Entity.EntityState.Modified;
            try { await db.SaveChangesAsync(); }
            catch (DbUpdateConcurrencyException)
            {
                if (!ContactoExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }
            return StatusCode(HttpStatusCode.NoContent);

        }
        [ResponseType(typeof(Tipo))]
        public async Task<IHttpActionResult> DeleteTipo(int id)
        {
            Tipo eliminado = await db.Tipo.FindAsync(id);
            if (eliminado == null)
            {
                return NotFound();
            }
            db.Tipo.Remove(eliminado);
            await db.SaveChangesAsync();
            return Ok(eliminado);

        }

        public bool ContactoExists(int id)
        {
            return db.Tipo.Count(e => e.TipoId == id) > 0;
        }
    }
}
