﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using TodoTicketProyectoCompuesto.Models;

namespace TodoTicketProyectoCompuesto.Controllers
{
    public class LoginController : Controller
    {
        // GET: Login
        public ActionResult Index()
        {
            return View();
        }


        private WebTicketMejoradoContext db = new WebTicketMejoradoContext();
        [HttpPost]
        public async Task<ActionResult> Login(string item1, string item2)
        {
            Usuario usuario = await db.Usuario.Where(a => a.Nombre.Equals(item1) && a.Password.Equals(item2)).FirstOrDefaultAsync();
            HttpCookie loginCoki = new HttpCookie(name: "login");

            if (usuario != null)
            {
                if (usuario.TipoUsuarioId == 2)
                {
                    loginCoki.Values["nombre"] = usuario.Nombre;
                    loginCoki.Values["id"] = usuario.UsuarioId.ToString();
                    Response.SetCookie(loginCoki);
                    return Json(new { result = "usuario", url = Url.Action("Index", "Usuarios") });

                }
                else {
                    loginCoki.Values["nombre"] = usuario.Nombre;
                    loginCoki.Values["id"] = usuario.UsuarioId.ToString();
                    Response.SetCookie(loginCoki);
                    return Json(new { result = "Admin", url = Url.Action("Index", "Usuarios") });
                }
                

            }
            return Json(new { result = "Incorrecto" });
        }
    }
}