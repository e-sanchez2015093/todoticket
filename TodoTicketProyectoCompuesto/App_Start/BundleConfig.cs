﻿using System.Web;
using System.Web.Optimization;

namespace TodoTicketProyectoCompuesto
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));
            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                      "~/Scripts/knockout-{version}.js"));
            bundles.Add(new ScriptBundle("~/bundles/miaplicacion").Include(
            "~/Scripts/main.js"));
            bundles.Add(new ScriptBundle("~/bundles/miaplicacion2").Include(
                    "~/Scripts/tipos.js"));
            bundles.Add(new ScriptBundle("~/bundles/miaplicacion3").Include(
                        "~/Scripts/staff.js"));
            bundles.Add(new ScriptBundle("~/bundles/miaplicacion4").Include(
            "~/Scripts/evento.js"));
            bundles.Add(new ScriptBundle("~/bundles/miaplicacion5").Include(
            "~/Scripts/tipoTicket.js"));
            bundles.Add(new ScriptBundle("~/bundles/miaplicacion6").Include(
            "~/Scripts/ticket.js"));
            bundles.Add(new ScriptBundle("~/bundles/login").Include(
            "~/Scripts/login.js"));
            bundles.Add(new ScriptBundle("~/bundles/cookieJ").Include(
           "~/Scripts/jquery.cookie.js"));

            bundles.Add(new ScriptBundle("~/bundles/chart").Include(
            "~/Scripts/Chart.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/fullcalendarcss").Include(
                      "~/Content/themes/jquery.ui.all.css",
                      "~/Content/fullcalendar.css"));

            bundles.Add(new ScriptBundle("~/bundles/fullcalendarjs").Include(
                                "~/Scripts/jquery-ui-{version}.min.js",
                               "~/Scripts/moment.min.js",
                               "~/Scripts/fullcalendar.min.js"));


        }
    }
}
