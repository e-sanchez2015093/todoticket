namespace TodoTicketProyectoCompuesto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MasVendidoUpdate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MasVendidoes",
                c => new
                    {
                        MasVendidoId = c.Int(nullable: false, identity: true),
                        TicketId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.MasVendidoId)
                .ForeignKey("dbo.Tickets", t => t.TicketId, cascadeDelete: true)
                .Index(t => t.TicketId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.MasVendidoes", "TicketId", "dbo.Tickets");
            DropIndex("dbo.MasVendidoes", new[] { "TicketId" });
            DropTable("dbo.MasVendidoes");
        }
    }
}
