namespace TodoTicketProyectoCompuesto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class inicial1 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Carritoes",
                c => new
                    {
                        CarritoId = c.Int(nullable: false, identity: true),
                        Cantidad = c.Int(nullable: false),
                        TicketId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.CarritoId)
                .ForeignKey("dbo.Tickets", t => t.TicketId, cascadeDelete: true)
                .Index(t => t.TicketId);
            
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        TicketId = c.Int(nullable: false, identity: true),
                        EventoId = c.Int(nullable: false),
                        TipoTicketId = c.Int(nullable: false),
                        LugarDistribucionId = c.Int(nullable: false),
                        PromocionesId = c.Int(nullable: false),
                        TotalTickets = c.Int(nullable: false),
                        Precio = c.Single(nullable: false),
                        Contandor = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.TicketId)
                .ForeignKey("dbo.Eventoes", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.LugarDistribucions", t => t.LugarDistribucionId, cascadeDelete: true)
                .ForeignKey("dbo.Promociones", t => t.PromocionesId, cascadeDelete: true)
                .ForeignKey("dbo.TipoTickets", t => t.TipoTicketId, cascadeDelete: true)
                .Index(t => t.EventoId)
                .Index(t => t.TipoTicketId)
                .Index(t => t.LugarDistribucionId)
                .Index(t => t.PromocionesId);
            
            CreateTable(
                "dbo.Eventoes",
                c => new
                    {
                        EventoId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        Precio = c.Single(nullable: false),
                        Img = c.String(nullable: false, maxLength: 120),
                        Fecha = c.DateTime(nullable: false),
                        LugarEvento = c.String(nullable: false, maxLength: 40),
                        Descripcion = c.String(nullable: false, maxLength: 40),
                        UsuarioId = c.Int(nullable: false),
                        TipoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.EventoId)
                .ForeignKey("dbo.Tipoes", t => t.TipoId, cascadeDelete: true)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioId, cascadeDelete: true)
                .Index(t => t.UsuarioId)
                .Index(t => t.TipoId);
            
            CreateTable(
                "dbo.Tipoes",
                c => new
                    {
                        TipoId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.TipoId);
            
            CreateTable(
                "dbo.Usuarios",
                c => new
                    {
                        UsuarioId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        Password = c.String(nullable: false, maxLength: 20),
                        TipoUsuarioId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UsuarioId)
                .ForeignKey("dbo.TipoUsuarios", t => t.TipoUsuarioId, cascadeDelete: true)
                .Index(t => t.TipoUsuarioId);
            
            CreateTable(
                "dbo.TipoUsuarios",
                c => new
                    {
                        TipoUsuarioId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.TipoUsuarioId);
            
            CreateTable(
                "dbo.LugarDistribucions",
                c => new
                    {
                        LugarDistribucionId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.LugarDistribucionId);
            
            CreateTable(
                "dbo.Promociones",
                c => new
                    {
                        PromocionesId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        Descuento = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.PromocionesId);
            
            CreateTable(
                "dbo.TipoTickets",
                c => new
                    {
                        TipoTicketId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.TipoTicketId);
            
            CreateTable(
                "dbo.DetalleFacturas",
                c => new
                    {
                        DetalleFacturaId = c.Int(nullable: false, identity: true),
                        FacturaId = c.Int(nullable: false),
                        EventoId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.DetalleFacturaId)
                .ForeignKey("dbo.Eventoes", t => t.EventoId, cascadeDelete: true)
                .ForeignKey("dbo.Facturas", t => t.FacturaId, cascadeDelete: true)
                .Index(t => t.FacturaId)
                .Index(t => t.EventoId);
            
            CreateTable(
                "dbo.Facturas",
                c => new
                    {
                        FacturaId = c.Int(nullable: false, identity: true),
                        Total = c.Single(nullable: false),
                    })
                .PrimaryKey(t => t.FacturaId);
            
            CreateTable(
                "dbo.Productoes",
                c => new
                    {
                        ProductoId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        Precio = c.Single(nullable: false),
                        Descripcion = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.ProductoId);
            
            CreateTable(
                "dbo.Staffs",
                c => new
                    {
                        StaffId = c.Int(nullable: false, identity: true),
                        Nombre = c.String(nullable: false, maxLength: 20),
                        Sueldo = c.Int(nullable: false),
                        Direccion = c.String(nullable: false, maxLength: 20),
                    })
                .PrimaryKey(t => t.StaffId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DetalleFacturas", "FacturaId", "dbo.Facturas");
            DropForeignKey("dbo.DetalleFacturas", "EventoId", "dbo.Eventoes");
            DropForeignKey("dbo.Carritoes", "TicketId", "dbo.Tickets");
            DropForeignKey("dbo.Tickets", "TipoTicketId", "dbo.TipoTickets");
            DropForeignKey("dbo.Tickets", "PromocionesId", "dbo.Promociones");
            DropForeignKey("dbo.Tickets", "LugarDistribucionId", "dbo.LugarDistribucions");
            DropForeignKey("dbo.Tickets", "EventoId", "dbo.Eventoes");
            DropForeignKey("dbo.Eventoes", "UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Usuarios", "TipoUsuarioId", "dbo.TipoUsuarios");
            DropForeignKey("dbo.Eventoes", "TipoId", "dbo.Tipoes");
            DropIndex("dbo.DetalleFacturas", new[] { "EventoId" });
            DropIndex("dbo.DetalleFacturas", new[] { "FacturaId" });
            DropIndex("dbo.Usuarios", new[] { "TipoUsuarioId" });
            DropIndex("dbo.Eventoes", new[] { "TipoId" });
            DropIndex("dbo.Eventoes", new[] { "UsuarioId" });
            DropIndex("dbo.Tickets", new[] { "PromocionesId" });
            DropIndex("dbo.Tickets", new[] { "LugarDistribucionId" });
            DropIndex("dbo.Tickets", new[] { "TipoTicketId" });
            DropIndex("dbo.Tickets", new[] { "EventoId" });
            DropIndex("dbo.Carritoes", new[] { "TicketId" });
            DropTable("dbo.Staffs");
            DropTable("dbo.Productoes");
            DropTable("dbo.Facturas");
            DropTable("dbo.DetalleFacturas");
            DropTable("dbo.TipoTickets");
            DropTable("dbo.Promociones");
            DropTable("dbo.LugarDistribucions");
            DropTable("dbo.TipoUsuarios");
            DropTable("dbo.Usuarios");
            DropTable("dbo.Tipoes");
            DropTable("dbo.Eventoes");
            DropTable("dbo.Tickets");
            DropTable("dbo.Carritoes");
        }
    }
}
