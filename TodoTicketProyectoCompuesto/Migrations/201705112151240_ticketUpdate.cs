namespace TodoTicketProyectoCompuesto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ticketUpdate : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Tickets", "Contandor");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tickets", "Contandor", c => c.Int(nullable: false));
        }
    }
}
