﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class MasVendido
    {
        public int MasVendidoId { get; set; }
        [Required]
        public int TicketId { get; set; }
        public Ticket Ticket { get; set; }


    }
}