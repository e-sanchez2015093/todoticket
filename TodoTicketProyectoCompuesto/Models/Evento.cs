﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class Evento
    {
        public int EventoId { get; set; }
        [Required, StringLength(20)]
        public string Nombre { get; set; }
        [Required]
        public float Precio { get; set; }
        [Required, StringLength(120)]
        public string Img { get; set; }
        [Required]
        public DateTime Fecha { get; set; }
        [Required, StringLength(40)]
        public string LugarEvento { get; set; }
        [Required, StringLength(40)]
        public string Descripcion { get; set; }
        [Required]
        public int UsuarioId { get; set; }
        public Usuario Usuario { get; set; }    

        [Required]
        public int TipoId { get; set; }
        public Tipo Tipo { get; set; }


    }
}