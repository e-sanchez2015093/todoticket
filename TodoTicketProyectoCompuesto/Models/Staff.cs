﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class Staff
    {
        public int StaffId { get; set; }
        [Required, StringLength(20)]
        public string Nombre { get; set; }
        [Required]
        public int Sueldo { get; set; }
        [Required, StringLength(20)]
        public string Direccion { get; set; }
    }
}