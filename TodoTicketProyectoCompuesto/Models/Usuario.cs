﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        [Required, StringLength(20)]
        public string Nombre { get; set; }
        [Required, StringLength(20)]
        public string Password { get; set; }
        [Required]
        public int TipoUsuarioId { get; set; }
        public TipoUsuario TipoUsuario { get; set; }

    }
}