﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class LugarDistribucion
    {
        public int LugarDistribucionId { get; set; }
        [Required, StringLength(20)]
        public string Nombre { get; set; }
    }
}