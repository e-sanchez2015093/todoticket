﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class DetalleFactura
    {
        public int DetalleFacturaId { get; set; }

        [Required]
        public int FacturaId { get; set; }

        public Factura Factura { get; set; }

        [Required]
        public int EventoId { get; set; }
        public Evento Evento { get; set; }

    }
}