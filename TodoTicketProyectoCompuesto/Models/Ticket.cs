﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class Ticket
    {
        public int TicketId { get; set; }
        [Required]
        public int EventoId { get; set; }
        public Evento Evento { get; set; }

        [Required]
        public int TipoTicketId { get; set; }
        public TipoTicket TipoTicket { get; set; }

        [Required]
        public int LugarDistribucionId { get; set; }
        public LugarDistribucion LugarDistribucion { get; set; }

        [Required]
        public int PromocionesId { get; set; }
        public Promociones Promociones { get; set; }
        [Required]
        public int TotalTickets { get; set; }
        [Required]
        public float Precio { get; set; }
        //public int Contandor { get; set; }
    }
}