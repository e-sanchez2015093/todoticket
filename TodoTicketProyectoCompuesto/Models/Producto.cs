﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class Producto
    {
        public int ProductoId { get; set; }
        [Required, StringLength(20)]
        public string Nombre { get; set; }
        [Required]
        public float Precio { get; set; }
        [Required, StringLength(50)]
        public string Descripcion { get; set; }
    }
}