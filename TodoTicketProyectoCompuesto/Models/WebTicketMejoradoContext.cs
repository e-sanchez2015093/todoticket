namespace TodoTicketProyectoCompuesto.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class WebTicketMejoradoContext : DbContext
    {
        // Your context has been configured to use a 'WebTicketMejoradoContext' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'TodoTicketProyectoCompuesto.Models.WebTicketMejoradoContext' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'WebTicketMejoradoContext' 
        // connection string in the application configuration file.
        public WebTicketMejoradoContext()
            : base("name=WebTicketMejoradoContext")
        {
        }


        public DbSet<Usuario> Usuario { get; set; }
        public DbSet<Tipo> Tipo { get; set; }
        public DbSet<Evento> Evento { get; set; }
        public DbSet<Factura> Factura { get; set; }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<TipoTicket> TipoTicket { get; set; }
        public DbSet<Ticket> Ticket { get; set; }
        public DbSet<DetalleFactura> DetalleFactura { get; set; }


        public DbSet<LugarDistribucion> LugarDistribucion { get; set; }

        public DbSet<Promociones> Promociones { get; set; }

        public DbSet<Carrito> Carrito { get; set; }
        public DbSet<Staff> Staff { get; set; }
        public DbSet<TipoUsuario> TipoUsuario { get; set; }
        public DbSet<MasVendido> MasVendido { get; set; }


        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}