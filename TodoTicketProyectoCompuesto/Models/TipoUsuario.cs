﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class TipoUsuario
    {
        public int TipoUsuarioId { get; set; }
        [Required]
        public string Nombre { get; set; }

    }
}