﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TodoTicketProyectoCompuesto.Models
{
    public class Factura
    {
        public int FacturaId { get; set; }
        [Required]
        public float Total { get; set; }
    }
}