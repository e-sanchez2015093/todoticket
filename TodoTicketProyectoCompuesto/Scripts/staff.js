﻿var Viewmodel = function () {
    var main = this;
    var staffUri = 'api/Staff/';
    main.staff = ko.observableArray();
    main.error = ko.observable();
    main.staffCargado = ko.observable();
    main.staffNuevo = {
        Nombre: ko.observable(),
        Sueldo: ko.observable(),
        Direccion: ko.observable()

    }
    main.cargar = function (item) {
        console.log(JSON.stringify(item));
        main.staffCargado(item);
    }
    main.editar = function (formElment) {
        var StaffEditado = {
            StaffId: main.staffCargado().StaffId,
            Nombre: main.staffCargado().Nombre,
            Sueldo: main.staffCargado().Sueldo,
            Direccion: main.staffCargado().Direccion
        }
        console.log(StaffEditado);
        var uri = staffUri + StaffEditado.StaffId;
        ajaxHelper(uri, 'PUT', StaffEditado)
        .done(function (data) {
            getAllStaffs();
            $('#myModalStaffEditar').modal('hide');
        });
    }
    main.eliminar = function (item) {
        var id = item.StaffId;
        var uri = staffUri + id;
        ajaxHelper(uri, 'Delete').done(function () {
            getAllStaffs();
        });

    }
    main.agregar = function (formElement) {
        var staf = {
            Nombre: main.staffNuevo.Nombre(),
            Sueldo: main.staffNuevo.Sueldo(),
            Direccion: main.staffNuevo.Direccion()
        }
        console.log(staf);
        ajaxHelper(staffUri, 'POST', staf)
        .done(function (data) {
            getAllStaffs();
            $('#myModalStaffNuevo').modal('hide');
        });

    }


    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null

        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);

        });

    }
    function getAllStaffs() {
        ajaxHelper(staffUri, 'GET')
        .done(function (data) {
            limpiar();
            main.staff(data);
        });
    }
    function limpiar() {
        main.staffNuevo.Nombre(null);
        main.staffNuevo.Sueldo(null);
        main.staffNuevo.Direccion(null);
    }
    getAllStaffs();


}
$(document).ready(function () {
    var viewModel = new Viewmodel();
    ko.applyBindings(viewModel);

    $(".btn-guardar").click(function () {
        console.log("Prueba");
        var form = $("#form-staff");
        viewModel.agregar(form);
    });



});