﻿var Viewmodel = function () {
    var main = this;
    var usuarioUri = 'api/Usuario/';
    var tipoUsuarioUri = 'api/TipoUsuario/';
    main.usuarios = ko.observableArray();
    main.tipoUsuarios = ko.observableArray();
    main.error = ko.observable();
    main.usuarioCargado = ko.observable();
    main.usuarioNuevo = {
        Nombre: ko.observable(),
        Password: ko.observable(),
        TipoUsuario: ko.observable()
    }
    main.cargar = function (item) {
        console.log(JSON.stringify(item));
        main.usuarioCargado(item);
    }

    main.editar = function (formElemnt) {
        var UsuarEditado = {
            UsuarioId: main.usuarioCargado().UsuarioId,
            Nombre: main.usuarioCargado().Nombre,
            Password: main.usuarioCargado().Password,
            TipoUsuarioId: main.usuarioCargado().TipoUsuario.TipoUsuarioId
        }
        console.log(UsuarEditado);
        var uri = usuarioUri + UsuarEditado.UsuarioId;
        ajaxHelper(uri, 'PUT', UsuarEditado)
        .done(function (data) {
            getAllUsuarios();
            $('#myModalUsuarioEditar').modal('hide');
        });
    }



    main.eliminar = function (item) {
        var id = item.UsuarioId;
        var uri = usuarioUri + id;
        ajaxHelper(uri, 'Delete').done(function () {
            getAllUsuarios();
        });
    }


    main.agregar = function (formElement) {
        var usuario = {
            Nombre: main.usuarioNuevo.Nombre(),
            Password: main.usuarioNuevo.Password(),
            TipoUsuarioId: main.usuarioNuevo.TipoUsuario().TipoUsuarioId
        }
        console.log(usuario);
        ajaxHelper(usuarioUri, 'POST', usuario)
        .done(function (data) {
            getAllUsuarios();
            $('#myModalUsuarioNuevo').modal('hide');
        });
    }
    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null

        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);

        });

    }

    function getAllUsuarios() {
        ajaxHelper(usuarioUri, 'GET')
        .done(function (data) {
            limpiar();
            main.usuarios(data);
        });
    }
    function getAllTipoUsuarios() {
        ajaxHelper(tipoUsuarioUri, 'GET')
        .done(function (data) {
      
             main.tipoUsuarios(data);
        });

    }
    getAllTipoUsuarios();
    getAllUsuarios();

    function limpiar() {
        main.usuarioNuevo.Nombre(null);
        main.usuarioNuevo.Password(null);

    }

}

$(document).ready(function () {
    var viewModel = new Viewmodel();
    ko.applyBindings(viewModel);

    $(".btn-guardar").click(function () {
        console.log("Prueba");
        var form = $("#form-usuario");
        viewModel.agregar(form);
    });


});