﻿var ViewModel = function () {
    var main = this;
    var tipoTiketUri = 'api/TipoTicket/';
    main.tipoTicket = ko.observableArray();
    main.error = ko.observable();
    main.tipoTicketCargado = ko.observable();
    main.tipoTicketNuevo = {
        Nombre: ko.observable()

    }
    main.agregar = function (formElemtt) {
        var tipoTicket = {
            Nombre : main.tipoTicketNuevo.Nombre()
        }
        console.log(tipoTicket);
        ajaxHelper(tipoTiketUri, 'POST', tipoTicket)
         .done(function (data) {
             getAllTipoTickets();
             $('#myModaltipoTicketNuevo').modal('hide');
         });

    }
    main.cargar = function (item){
        console.log(JSON.stringify(item));
        main.tipoTicketCargado(item);
    }
    main.editar = function (formElemt) {
        var eventoEditado = {
            TipoTicketId: main.tipoTicketCargado().TipoTicketId,
            Nombre: main.tipoTicketCargado().Nombre
        }
        var uri = tipoTiketUri + eventoEditado.TipoTicketId;
        ajaxHelper(uri, 'PUT', eventoEditado)
        .done(function (data) {
            getAllTipoTickets();
            $('#myModaltipoTicketEditar').modal('hide');
        })

    }


    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }
    function getAllTipoTickets() {
        ajaxHelper(tipoTiketUri, 'GET')
        .done(function (data) {
            main.tipoTicket(data);
        });
    }
    getAllTipoTickets();


}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

});