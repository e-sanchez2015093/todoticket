﻿var ViewModel = function () {
    var main = this;
    var ticketUri = 'api/Ticket/';
    var eventoUri = 'api/Evento/';
    var tipoTicketUri = 'api/TipoTicket/';
    var lugarDistribucionUri = 'api/LugarDistribucion/';
    var promocionesUri = 'api/Promociones/';

    main.ticketCargado = ko.observable();
    main.evento = ko.observableArray();
    main.lugarDistribucion = ko.observableArray();
    main.promocion = ko.observableArray();
    main.tipoTicket = ko.observableArray();
    main.ticket = ko.observableArray();
    main.error = ko.observable();
    main.ticketNuevo = {
        Evento:ko.observable(),
        TipoTicket: ko.observable(),
        LugarDistribucion: ko.observable(),
        Promocion: ko.observable(),
        TotalTickets: ko.observable(),
        Precio: ko.observable(),


    }
    main.agregar = function (formElement) {
        var ticket = {
           EventoId:main.ticketNuevo.Evento().EventoId,
           TipoTicketId: main.ticketNuevo.TipoTicket().TipoTicketId,
           LugarDistribucionId: main.ticketNuevo.LugarDistribucion().LugarDistribucionId,
           PromocionesId: main.ticketNuevo.Promocion().PromocionesId,
           TotalTickets: main.ticketNuevo.TotalTickets(),
           Precio: main.ticketNuevo.Precio()


        }
        console.log(ticket);
        ajaxHelper(ticketUri, 'POST', ticket)
        .done(function (data) {
            getAllTickets();
            $('#myModalTicketNuevo').modal('hide');
        });

    }
    main.cargar = function(item) {
        console.log(JSON.stringify(item));
        main.ticketCargado(item);
    }

    main.editar = function (formElemnt) {
        var ticketEditado = {
            TicketId: main.ticketCargado().TicketId,
            EventoId: main.ticketCargado().Evento.EventoId,
            TipoTicketId: main.ticketCargado().TipoTicket.TipoTicketId,
            LugarDistribucionId: main.ticketCargado().LugarDistribucion.LugarDistribucionId,
            PromocionesId: main.ticketCargado().Promocion.PromocionesId,
            TotalTickets: main.ticketCargado().TotalTickets,
            Precio: main.ticketCargado().Precio
        }
        console.log(ticketEditado);
        var uri = ticketUri + ticketEditado.TicketId;
        ajaxHelper(uri, 'PUT', ticketEditado)
        .done(function (data) {
            getAllTickets();
            $('#myModalTicketEditar').modal('hide');

        })

    }

    //////////////////////////////////////////////////////////



    main.grafica = function () {

        var arreglo = [];
        var arregloNombres = [];
        $.each(main.ticket(), function (key, value) {
            arreglo.push(value.TotalTickets);
        });

        $.each(main.ticket(), function (key, value) {
            arregloNombres.push(value.Evento.Nombre);
        });

        var data = {
            labels: arregloNombres,
            datasets: [
               {
                   label: "Eventos",
                   backgroundColor: [
                       'rgba(255, 99, 132, 0.2)',
                       'rgba(54, 162, 235, 0.2)',
                       'rgba(255, 206, 86, 0.2)',
                       'rgba(75, 192, 192, 0.2)',
                       'rgba(153, 102, 255, 0.2)',
                       'rgba(255, 159, 64, 0.2)'
                   ],
                   borderColor: [
                       'rgba(255,99,132,1)',
                       'rgba(54, 162, 235, 1)',
                       'rgba(255, 206, 86, 1)',
                       'rgba(75, 192, 192, 1)',
                       'rgba(153, 102, 255, 1)',
                       'rgba(255, 159, 64, 1)'
                   ],
                   borderColor: [
                       'rgba(255,99,132,1)',
                       'rgba(54, 162, 235, 1)',
                       'rgba(255, 206, 86, 1)',
                       'rgba(75, 192, 192, 1)',
                       'rgba(153, 102, 255, 1)',
                       'rgba(255, 159, 64, 1)'
                   ],
                   borderWidth: 1,
                   data: arreglo,
               }
            ]
        };

        var ctx = $('#charts');

        new Chart(ctx, {
            type: "bar",
            data: data,
            options: {
                scales: {
                    xAxes: [{
                        stacked: true
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                },
                responsive: false
            }
        });

    }
















    /////////////////////////////////////////////////////////













    function getAllTickets() {
        ajaxHelper(ticketUri, 'GET')
        .done(function (data) {
            main.ticket(data);
        });

    }
    getAllTickets();
    function getAllEventos() {
      ajaxHelper(eventoUri, 'GET')
      .done(function (data) {
       main.evento(data);
       });

    }
    getAllEventos();
    function getAllTipoTickets() {
        ajaxHelper(tipoTicketUri, 'GET')
        .done(function (data) {
            main.tipoTicket(data);
        });

    }
    getAllTipoTickets();
    function getAllLugarDistribucion() {
        ajaxHelper(lugarDistribucionUri, 'GET')
        .done(function (data) {
            main.lugarDistribucion(data);
        });

    }
    getAllLugarDistribucion();
    function getAllLugarPromociones() {
        ajaxHelper(promocionesUri, 'GET')
        .done(function (data) {
            main.promocion(data);
        });

    }
    getAllLugarPromociones();



    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null
        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);
        });
    }




}
$(document).ready(function () {
    var viewModel = new ViewModel();
    ko.applyBindings(viewModel);

});