﻿var Viewmodel = function () {
    var main = this;
    var eventoUri = 'api/Evento/';
    var tipoUri = 'api/Tipo/';
   // var masVendioUri = 'api/mas/Ticket';
    main.evento = ko.observableArray();
    main.tipo = ko.observableArray();
    main.masVendido = ko.observableArray([]);
    main.vendis = ko.observableArray([]);
    main.puro = ko.observableArray([]);
    main.calendario = ko.observable();
    main.usuario = ko.observable();
    main.tab = ko.observableArray();
    main.moda = ko.observable();
    main.mediana = ko.observable();
    main.media = ko.observable();
    main.popular = ko.observable();
    main.error = ko.observable();
    main.calendario = ko.observable();
    main.calendarioPorUsuario = ko.observable();
    main.eventoCargado = ko.observable();
    main.eventoNuevo = {
        Nombre: ko.observable(),
        Precio: ko.observable(),
        Img: ko.observable(),
        Fecha: ko.observable(),
        LugarEvento: ko.observable(),
        Descripcion: ko.observable(),
        Tipo: ko.observable()


    }
    main.cargar = function (item) {
        console.log(JSON.stringify(item));
        main.eventoCargado(item);
    }

    main.editar = function (formElemnt) {
        var eventoEdiatado = {
            EventoId: main.eventoCargado().EventoId,
            Nombre: main.eventoCargado().Nombre,
            Precio: main.eventoCargado().Precio,
            Img: main.eventoCargado().Img,
            Fecha: main.eventoCargado().Fecha,
            LugarEvento: main.eventoCargado().LugarEvento,
            Descripcion: main.eventoCargado().Descripcion,
            TipoId: main.eventoCargado().Tipo.TipoId,
            UsuarioId: main.eventoCargado().UsuarioId

        }
        console.log(eventoEdiatado);
        var uri = eventoUri + eventoEdiatado.EventoId;
        ajaxHelper(uri, 'PUT', eventoEdiatado)
        .done(function (data) {
            getAllEventos();
            $('#myModalEventoEditar').modal('hide');

        })
    }
    main.eliminar = function (item) {
        var id = item.EventoId;
        var uri = eventoUri + id;
        ajaxHelper(uri, 'DELETE').done(function () {
            getAllEventos();
        });
    }

    main.agregar = function (formElemnt) {
        console.log("Cookie");
        console.log("Cookie" + JSON.stringify($.cookie("login")));

        var co = JSON.stringify($.cookie("login"));
        var cookieArreglo = co.split("=");
        console.log("Entro al split0");
        console.log(cookieArreglo[0]);
        console.log("Entro al split1");
        console.log(cookieArreglo[1]);
        console.log("Entro al split2");
        console.log(cookieArreglo[2].substring(0,cookieArreglo[2].length-1));
       // var arreglo2 = cookieArreglo[1];
        //var sp = arreglo2.split("&");
        //console.log(sp[1]);
        var evento = {
            Nombre: main.eventoNuevo.Nombre(),
            Precio: main.eventoNuevo.Precio(),
            Img: main.eventoNuevo.Img(),
            Fecha: main.eventoNuevo.Fecha(),
            LugarEvento:main.eventoNuevo.LugarEvento(),
            Descripcion: main.eventoNuevo.Descripcion(),
            TipoId: main.eventoNuevo.Tipo().TipoId,
            UsuarioId: parseInt(cookieArreglo[2].substring(0, cookieArreglo[2].length - 1))


        }
        console.log(evento);
        ajaxHelper(eventoUri, 'POST', evento)
            .done(function (data) {
                getAllEventos();
                $('#myModalEventoNuevo').modal('hide');
            });


    }


    ////////////////////////////////////////////////////////////////



   































    ///////////////////////////////////////////////////////////////


    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null

        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);

        });

    }
    function getAllEventos() {
        ajaxHelper(eventoUri, 'GET')
        .done(function (data) {
            main.evento(data);
        });
    }
    getAllEventos();

    function getAllTipos() {
        ajaxHelper(tipoUri, 'GET')
        .done(function (data) {
            main.tipo(data);
        });
    }
    getAllTipos();

    main.calendario = function () {
        var arreglo = [];
        var arreglo1 = [];
        getAllEventos();
        console.log("Entro al calendario");
       // console.log("Cookie");
        //console.log("Cookie" + JSON.stringify($.cookie("login")));
        
        //var co = JSON.stringify($.cookie("login"));
        //var cookieArreglo = co.split("=");
        //console.log("Entro al split");
        //console.log(cookieArreglo[1]);
        //var arreglo2 = cookieArreglo[1];
        //var sp = arreglo2.split("&");
        //console.log(sp[0]);

        $.each(main.evento(), function (key, value) {
            console.log("Entro al foreaach");
            
            arreglo1.push({
                title: value.Nombre,

                start: value.Fecha.toString().split("T")[0],
                color: 'tomato'

            });
        });
            var my_events = {
                events: arreglo1,
                color: '#55B2DA',
                textColor: '#3c3c3c'
            };

            /*$('#calendario').fullCalendar({
                defaultView: 'month',
                eventSources: my_events
            });*/
            $('#calendario').fullCalendar({
                defaultView: 'month',
                eventSources: my_events

            });


        


    }
    main.calendarioPorUsuario = function () {
        var arreglo = [];
        var arreglo1 = [];
        getAllEventos();
        console.log("Entro al calendario");
        console.log("Cookie");
        var co = JSON.stringify($.cookie("login"));
        var cookieArreglo = co.split("=");
        console.log("Entro al split");
        console.log(cookieArreglo[1]);
        var arreglo2 = cookieArreglo[1];
        var sp = arreglo2.split("&");
        console.log(sp[0]);
        $.each(main.evento(), function (key, value) {
            console.log("Entro al foreaach");
            if (value.Usuario.Nombre == sp[0]) {
                console.log("Entro al if");
                   arreglo1.push({
                title: value.Nombre,

                start: value.Fecha.toString().split("T")[0],
                color: 'tomato'

            });
            }
            

        });
        var my_events = {
            events: arreglo1,
            color: '#55B2DA',
            textColor: '#3c3c3c'
        };

        /*$('#calendario').fullCalendar({
            defaultView: 'month',
            eventSources: my_events
        });*/
        $('#calendarioU').fullCalendar({
            defaultView: 'month',
            eventSources: my_events

        });



    }
   
}
$(document).ready(function () {
    var viewModel = new Viewmodel();
    ko.applyBindings(viewModel);

    /*$('#calendario').fullCalendar({
        theme: true,
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultView: 'agendaDay',
        editable:true,
        allDaySlot:false,
        selectable:true,
        slotMinutes:15,
        events:""

    });*/



});