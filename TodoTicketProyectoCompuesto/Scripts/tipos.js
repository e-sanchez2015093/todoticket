﻿var Viewmodel = function () {
    var main = this;
    var tipoUri = 'api/Tipo/';
    main.tipos = ko.observableArray();
    main.error = ko.observable();
    main.tipoCargado = ko.observable();
    main.tipoNuevo = {
        Nombre: ko.observable()
    }
    main.cargar = function (item) {
        console.log(JSON.stringify(item));
        main.tipoCargado(item);

    }
    main.editar = function (formElement) {
        var TipoEditado = {
            TipoId: main.tipoCargado().TipoId,
            Nombre: main.tipoCargado().Nombre

        }
        console.log(TipoEditado);
        var uri = tipoUri + TipoEditado.TipoId;
        ajaxHelper(uri, 'PUT', TipoEditado)
        .done(function (data) {
            getAllTipos();
            $('#myModalTipoEditar').modal('hide');

        });

    }
    main.eliminar = function (item) {
        var id = item.TipoId;
        var uri = tipoUri + id;
        ajaxHelper(uri, 'Delete').done(function () {
            getAllTipos();
        })

    }

    main.agregar = function (formElement) {
        var tip = {
            Nombre: main.tipoNuevo.Nombre()

        }
        console.log(tip);
        ajaxHelper(tipoUri, 'POST', tip)
        .done(function (data) {
            getAllTipos();
            $('#myModalTipoNuevo').modal('hide');
        });

    }

    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null

        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);

        });

    }
    function getAllTipos() {
        ajaxHelper(tipoUri, 'GET')
        .done(function (data) {
            limpiar();
            main.tipos(data);

        });
    }
    getAllTipos();

    function limpiar() {
        main.tipoNuevo.Nombre(null);

    }

}
$(document).ready(function () {
    var viewModel = new Viewmodel();
    ko.applyBindings(viewModel);

    $(".btn-guardar").click(function () {
        console.log("Prueba");
        var form = $("#form-tipo");
        viewModel.agregar(form);
    });


});