﻿var Viewmodel = function () {
    var main = this;
    var loginUri = '/Login/Login/';
    main.error = ko.observable();
    
    main.Nombre = ko.observable(),
    main.Password = ko.observable()

    
    main.mensaje = ko.observable();
    main.inicio = function (formElemnt) {
        var usuario = {
            item1: main.Nombre(),
            item2: main.Password()

        }
        console.log(JSON.stringify(usuario));
        $.ajax({
            url: '/Login/Login/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(usuario),
            success: function (data) {
                if (data.result === "Incorrecto") {
                    console.log("Incorrecto");
                    main.mensaje(data.result);
                }
                if (data.result === "usuario") {
                    window.location = data.url;
                    console.log("Usuario correcto");
                }
                if (data.result === "Admin") {
                    console.log("Administrador correcto");
                    window.location = data.url;
                }
            }
        });
    }
    function ajaxHelper(uri, method, data) {
        main.error('');
        return $.ajax({
            url: uri,
            type: method,
            dataType: 'json',
            contentType: 'application/json',
            data: data ? JSON.stringify(data) : null

        }).fail(function (jqXHR, textStatus, errorThrown) {
            main.error(errorThrown);

        });

    }
}
$(document).ready(function () {
    var viewModel = new Viewmodel;
    ko.applyBindings(viewModel);


});